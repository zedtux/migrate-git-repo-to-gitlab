# Migrate your Git repo to Gitlab

Given you'd like to migrate from GitHub to Gitlab, here is the script I wrote
in order to migrate quickly.

This script remove all the code and write a simple `README.md` file informing
people that you've migrated this repo to GitLab with a link. :)
